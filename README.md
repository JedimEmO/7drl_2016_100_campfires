# 100 campfires RL

## Project information

7DRL Roguelike competition entry created by Mathias Myrland in the period 6th. to 13th. of March 2016

### Building and running
You will need ```docker``` and ```nodejs + npm``` installed. To build and deploy, execute the following from the root folder.
After the build, the game should be accessible at http://localhost:8080

```
pushd web
npm install
node node_modules/jspm/cli.js install -y
popd
cd package
./build.sh
./run.sh
```

### Licensing

MIT

## Core gameplay and engine goals

* Hexagonal tile map
* Basic traditional RL bump-to-attack combat system
* Exploration enforced by the need for fuel - should introduce a basic risk for the player (woods are dangerous!)
* Day/Night cycle - nights without campfires should be bad.
* Implement a mechanic to light campfires, which will spawn/attract enemies, possibly progressively stronger waves
as the game progresses
* If no campfire is lit during night, really bad monsters should have a chance to appear
* Nights should requrie bigger and bigger campfires as the game progresses
* Victory condition of lighting 100 campfires
* Failure condition of death

## Engine choices

### Technology choices

The engine will be written in ES6, rendering to the canvas. For transpilation, dependency management and other
glorious things related to the browser world, it will use babel, JSPM/systemjs and of course npm/nodejs.

The package step will provide a web server and the game files hosted in a neatly packed ready to deploy docker image.
