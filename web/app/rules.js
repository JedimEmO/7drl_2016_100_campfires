"use strict";

import {GameEngine} from "./engine/engine";
import {createGameOverlay} from "./gui/overlay";
import {PlayerInputHandler} from "./entities/playerEntity";
import {PlayerEntity} from "./entities/playerEntity";

export class GameRules {
}

GameRules.campfireCost = 3;
GameRules.nightTurns = 20;
GameRules.campfireTurnLength = 15;
GameRules.nightsToWin = 100;

export class GameState {
    constructor(tileset, sprites, entityFactory, itemDatabase) {
        GameState.instance = this;
        GameState.tileset = tileset;
        GameState.sprites = sprites;
        GameState.entityFactory = entityFactory;
        GameState.itemDatabase = itemDatabase;
        // Create the input handler, which tracks the keyboard state
        GameState.inputHandler = new PlayerInputHandler(sprites);
        GameState.initialize(this);
    }

    static initialize(inst) {
        inst.nights = 0;
        inst.nightTurns = 0;
        inst.result = 0;

        let onBeforeStart = (engine) => {
            // Start polling for player input
            inst.stopInput = setInterval(() => { GameState.inputHandler.step(engine);}, 80);
        };

        inst.engine = new GameEngine(document.getElementById("gameCanvas"), GameState.tileset, GameState.sprites, GameState.entityFactory, GameState.itemDatabase, onBeforeStart, createGameOverlay, console);
        inst.engine.run();
    }

    static reset() {
        GameState.instance.engine.stop();
        window.clearInterval(GameState.instance.stopInput);
        PlayerEntity.nextAction = null;
        PlayerEntity.instance = null;

        GameState.initialize(GameState.instance);
    }

    static win() {
        if (GameState.instance.result == 0) {
            GameState.instance.result = 1;

            // Reset after 10 seconds
            setTimeout(()=> {
                GameState.reset();
            }, 5000);
        }
    }

    static gameOver() {
        if (GameState.instance.result == 0) {
            GameState.instance.result = -1;

            // Reset after 10 seconds
            setTimeout(()=> {
                GameState.reset();
            }, 5000);
        }
    }
}