"use strict";

import {Entity} from "../engine/entity";
import {GameEngine} from "../engine/engine";
import {EntityFactory} from "./entityFactory";
import {Vector} from "../engine/vector";
import {PlayerEntity, movementVectors} from "./playerEntity";
import {InventoryComponent} from "./components";

export class Tree extends Entity {
    constructor(sprite, cell, stump) {
        super(sprite, cell);

        this.stump = stump
    }

    onDeath() {
        super.onDeath();
        let engine = GameEngine.instance;
        // Create the stump entity, and add it to the map
        let e = engine.entityFactory.createEntity(this.stump, engine, this.cell);

        engine.map.addEntity(this.cell.pos, e);

        // Spawn some items!
        let db = engine.itemDatabase;

        let log = db.createItem("log");

        let mv = movementVectors(engine.sprites);

        for (let p of mv) {
            let np = this.cell.pos.copy().add(p.vec);
            let cell = engine.map.getCell(np.x, np.y);

            if (cell) {
                if (!cell.entity) {
                    cell.items.push(log);
                    break;
                } else {
                    let inv = cell.entity.getComponent(InventoryComponent.name);

                    if (inv && log.pickUp(cell.entity)) {
                        break;
                    }
                }
            }
        }

        // After felling a tree, there may spawn some protectors of the forrest!
        if (parseInt(Math.random() * 10) <= 6) {
            let pos = PlayerEntity.instance.cell.pos.copy();
            pos.add(new Vector(parseInt(Math.random() * 10 - 5), parseInt(Math.random() * 10 - 5)));
            let c = engine.map.getCell(pos.x, pos.y);

            if (c && !c.entity) {
                let monster = engine.entityFactory.createEntity("enemy_1", engine);
                engine.map.addEntity(new Vector(pos.x, pos.y), monster);
            }
        }
    }
}
