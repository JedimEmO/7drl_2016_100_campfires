"use strict";

import {Item} from "../engine/item";
import {InventoryComponent} from "./components";
import {FloatingTextSettings} from "../engine/gfx/overlay";

class InventoryItem extends Item {
    constructor() {
        super();
    }

    pickUp(entity) {
        super.pickUp(entity);

        let inv = entity.getComponent(InventoryComponent.name);

        if (inv) {

            let txt = new FloatingTextSettings(`Gained ${this.name}`);
            txt.rgba = [100, 255, 100];
            entity.emitFloatingText(txt);

            inv.addItem(this);

            return true;
        }
    }
}

export class ItemFactory {
    constructor(db) {
        this.itemDatabase = db;
    }

    createItem(id) {
        let data = this.itemDatabase.createItem(id);
        let ret = new InventoryItem();
        ret.id = id;
        Object.assign(ret, data);

        return ret;
    }
}
