"use strict";


import {Entity} from "app/engine/entity";
import {Vector} from "app/engine/vector";
import {Action,ActionQueue} from "app/engine/action/actionQueue";
import {MoveAction, AttackAction} from "./action";
import {PlayerEntity} from "./playerEntity";
import {findPath} from "app/engine/map/path";
import {GameEngine} from "app/engine/engine";

export class BasicEnemy extends Entity {
    constructor(sprite, cell, hitpoints) {
        super(sprite, cell);
    }

    takeTurn() {
        let pe = PlayerEntity.instance;

        if (!pe) {
            return 0;
        }

        let playerPos = pe.cell.pos;
        let myPos = this.cell.pos;

        let distToPlayer = playerPos.copy().subtract(myPos).length();

        if (distToPlayer < 20) {
            let map = GameEngine.instance.map;

            // Get the path to the player
            let pathToPlayer = findPath(map, myPos, playerPos, (map, pos) => {
                let cell = map.getCell(pos.x, pos.y);

                if (cell == pe.cell) {
                    return true;
                }

                if (cell) {
                    return cell.entity == null;
                }

                return false;
            }, 2000);

            let action = new Action(0);

            // Move towards the player
            if (pathToPlayer.length > 2) {
                let next = pathToPlayer[1];
                let nextCell = map.getCell(next.x, next.y);
                action = new MoveAction(25, map, this, this.cell, nextCell);
            } else if (pathToPlayer.length == 2) {
                // If within range of an attack on the player, do it!
                action = new AttackAction(this, pe);
            }

            return action.execute();
        }

        return 0;
    }

    onDeath() {
        super.onDeath();
        let engine = GameEngine.instance;

        // Enemies might drop potions
        let db = engine.itemDatabase;

        if (this.loot) {
            for (let loot of this.loot) {
                if (Math.random() <= loot.chance) {
                    let item = db.createItem(loot.item);
                    this.cell.items.push(item);
                }
            }
        }
    }
}
