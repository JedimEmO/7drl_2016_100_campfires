"use strict";

import {Actor, ActionQueue} from "../engine/action/actionQueue";
import {Entity, AttackableComponent} from "../engine/entity";
import {PlayerEntity} from "./playerEntity";
import {Tree} from "./tree";
import {BasicEnemy} from "./enemy";
import {Campfire} from "./campfire";
import {ChoppableComponent, InventoryComponent} from "./components";

/**
 * Factory producing entities based on a blueprint
 */
export class EntityFactory {
    constructor(dataSet) {
        this.blueprints = {};

        for (let ent of dataSet.entityBlueprints) {
            this.blueprints[ent.name] = ent;
        }
    }

    createEntity(name, engine, cell) {
        if (!this.blueprints.hasOwnProperty(name)) {
            throw `Attempting to create entity from invalid blueprint: ${name}`;
        }

        let ent = this.blueprints[name];
        let ret = {};

        if (ent.entityClass == "Player") {
            ret = new PlayerEntity(engine.sprites.getSprite(ent.sprite), engine, cell);
        } else if (ent.entityClass == "BasicEnemy") {
            ret = new BasicEnemy(engine.sprites.getSprite(ent.sprite), cell);
            ret.loot = ent.loot;
        } else if (ent.entityClass == "Tree") {
            ret = new Tree(engine.sprites.getSprite(ent.sprite), cell, ent.stump);
        } else if (ent.entityClass == "Campfire") {
            ret = new Campfire(engine.sprites.getSprite(ent.sprite), cell);
        } else {
            ret = new Entity(engine.sprites.getSprite(ent.sprite), cell);
        }

        //TODO: Component factory?
        if (ent.components) {
            for (let compName of Object.keys(ent.components)) {
                if (compName == "attackable") {
                    ret.addComponent(new AttackableComponent(ret, ent.components[compName].hitpoints));
                } else if (compName == "choppable") {
                    ret.addComponent(new ChoppableComponent(ret, ent.components[compName].strength));
                } else if (compName == "inventory") {
                    ret.addComponent(new InventoryComponent(ret));
                }
            }
        }

        ret.nextTurn = ActionQueue.tick+1;
        ret.physical = Object.assign(ent.physical);

        return ret;
    }
}
