"use strict";

import {Component} from "../engine/entity";
import {FloatingTextSettings} from "../engine/gfx/overlay";

/**
 * Component indicating that this entity can be chopped down by the player
 */
export class ChoppableComponent extends Component {
    constructor(entity, strength) {
        super(entity);

        this.strength = strength;
        this.currentStrength = strength;
    }

    chop() {
        this.currentStrength -= 5;

        let txt = new FloatingTextSettings("Chop...");
        txt.rgba = [100, 255, 100];
        this.entity.emitFloatingText(txt);

        if (this.currentStrength <= 0) {
            this.entity.kill();
        }
    }
}

export class InventoryComponent extends Component {
    constructor(entity) {
        super(entity);
        this.items = [];
    }

    addItem(item) {
        this.items.push(item);
    }

    /**
     * Takes the first item matching id out of the inventory and returns it
     * @param id
     */
    takeFirst(id) {
        for (let i of Object.keys(this.items)) {
            let itm = this.items[i];

            if (itm.id == id) {
                this.items.splice(i, 1);
                return itm;
            }
        }

        return null;
    }

    getItemCount(id) {
        let count = 0;

        for (let i of this.items) {
            if (i.id == id) {
                count++;
            }
        }

        return count;
    }
}
