"use strict";

import {Entity, AttackableComponent} from "app/engine/entity";
import {Vector} from "app/engine/vector";
import {Action,ActionQueue} from "app/engine/action/actionQueue";
import {MoveAction, AttackAction, ChopAction, QuaffAction, BuildCampfire} from "./action";
import {KeyboardInput} from "app/engine/input";
import {mapToScreen} from "app/engine/map/coordinates";
import {ChoppableComponent} from "./components";
import {GameState} from "../rules";

export class PlayerEntity extends Entity {
    constructor(sprite, engine, cell) {
        super(sprite, cell);
        this.engine = engine;
        PlayerEntity.instance = this;

    }

    isBlocking() {
        return true;
    }

    kill() {
        GameState.gameOver();
    }

    /**
     * If an action has been selected for the next turn, eat it and return its cost
     */
    processAction() {
        let cost = 0;

        if (PlayerEntity.nextAction) {
            cost = PlayerEntity.nextAction.cost;
            PlayerEntity.nextAction.execute();
            PlayerEntity.nextAction = null;
        }

        return cost;
    }

    takeTurn() {
        return this.processAction();
    }
}

PlayerEntity.nextAction = null;

class PlayerKeyboardInput extends KeyboardInput {
    constructor() {
        super();
    }

    up() {
        return this.isAnyPressed(this.inputKeyGroups.up);
    }

    down() {
        return this.isAnyPressed(this.inputKeyGroups.down);
    }

    left() {
        return this.isAnyPressed(this.inputKeyGroups.left);
    }

    right() {
        return this.isAnyPressed(this.inputKeyGroups.right);
    }

    quaff() {
        return this.isAnyPressed(this.inputKeyGroups.quaff);
    }

    action() {
        return this.isAnyPressed(this.inputKeyGroups.action);
    }

    help() {
        return this.isAnyPressed(this.inputKeyGroups.help);
    }

}

export function movementVectors(sprites) {
    return [
        {vec: new Vector(-1, 0), over: sprites.getSprite("arrow_up_right")},
        {vec: new Vector(-1, -1), over: sprites.getSprite("arrow_up")},
        {vec: new Vector(0, -1), over: sprites.getSprite("arrow_up_left")},
        {vec: new Vector(1, 0), over: sprites.getSprite("arrow_down_left")},
        {vec: new Vector(1, 1), over: sprites.getSprite("arrow_down")},
        {vec: new Vector(0, 1), over: sprites.getSprite("arrow_down_right")}
    ];
}

export class PlayerInputHandler {

    constructor(sprites) {
        this.sprites = sprites;
        this.movementVectors = movementVectors(sprites);

        this.playerOrientation = 0;

        this.input = new PlayerKeyboardInput();
    }

    step(engine) {
        if (!PlayerEntity.instance || PlayerEntity.nextAction) {
            return;
        }

        let pe = PlayerEntity.instance;

        let oldCell = pe.cell;
        let curPos = oldCell.pos.copy();
        let newPos = curPos.copy();

        let moveVec = this.movementVectors[this.playerOrientation % this.movementVectors.length].vec;

        if (this.input.up()) {
            newPos.add(moveVec);
        } else if (this.input.down()) {
            newPos.subtract(moveVec);
        } else if (this.input.left()) {
            this.playerOrientation++;
        } else if (this.input.right()) {
            this.playerOrientation--;
        }

        this.playerOrientation = (this.playerOrientation + this.movementVectors.length) % this.movementVectors.length;

        let newCell = engine.map.getCell(newPos.x, newPos.y);
        let moved = newPos.copy().subtract(curPos).length() > 0;

        // the coast is clear!
        if (moved > 0 && newCell && !newCell.entity) {
            PlayerEntity.nextAction = new MoveAction(20, engine.map, PlayerEntity.instance, oldCell, newCell);
        } else if (moved > 0 && newCell && newCell.entity && newCell.entity != pe) {
            // See if we can attack the next cells inhabitant
            let attack = newCell.entity.getComponent(AttackableComponent.name);
            let chop = newCell.entity.getComponent(ChoppableComponent.name);

            if (attack) {
                PlayerEntity.nextAction = new AttackAction(pe, newCell.entity);
            }

            if (chop) {
                PlayerEntity.nextAction = new ChopAction(pe, newCell.entity);
            }
        } else if (moved == 0 && this.input.quaff()) {
            // Quaff a potion if possible (The quaff action will pull a potion from the player inventory if possible
            PlayerEntity.nextAction = new QuaffAction(pe);
        } else if (moved == 0 && this.input.action()) {
            // Build a campfire if possible!
            let tp = curPos.copy().add(moveVec);
            let tCell = engine.map.getCell(tp.x, tp.y);

            if (!tCell.entity) {
                PlayerEntity.nextAction = new BuildCampfire(pe, tCell);
            }
        }

        if (this.input.help()) {
            GameState.instance.showHelp = !GameState.instance.showHelp;
        }

        // Replace the marker
        moveVec = this.movementVectors[this.playerOrientation % this.movementVectors.length].vec;

        let markerPos = mapToScreen(moveVec);
        markerPos.x += 1280/2;
        markerPos.y += 720/2;

        engine.overlay.setSprite("player_direction", this.movementVectors[this.playerOrientation].over, markerPos);
    }
}