"use strict";

import {Action} from "../engine/action/actionQueue";
import {AttackableComponent} from "../engine/entity";
import {ChoppableComponent, InventoryComponent} from "./components";
import {FloatingTextSettings} from "../engine/gfx/overlay";
import {Vector} from "../engine/vector";
import {GameRules, GameState} from "../rules";

export class MoveAction extends Action {
    constructor(cost, map, entity, src, dst) {
        super(cost);

        this.map = map;
        this.src = src;
        this.dst = dst;
        this.entity = entity;
    }

    execute() {
        if (!this.dst.entity) {
            // Atempt to pick up any items on the target cell
            if (this.dst.items.length) {
                for (let i of Object.keys(this.dst.items)) {
                    let item = this.dst.items[i];

                    if (item.pickUp(this.entity)) {
                        this.dst.items[i] = undefined;
                    }
                }
            }

            this.dst.items = this.dst.items.filter((v) => v != undefined);

            this.dst.entity = this.src.entity;
            this.src.entity = null;
            this.entity.cell = this.dst;

            return this.cost;
        }

        return 0;
    }
}

export class AttackAction extends Action {
    constructor(attacker, target) {
        super(10);

        this.attacker = attacker;
        this.target = target;
    }

    execute() {
        let cmp = this.target.getComponent(AttackableComponent.name);

        let txt = new FloatingTextSettings("3");

        txt.movement = new Vector(Math.random()*40-20, 50);

        this.target.emitFloatingText(txt);

        cmp.damage(3);

        return 10;
    }
}

/**
 * Swings at a tree, hoping to fell it. Be wary of defenders of the forrest
 */
export class ChopAction extends Action {
    constructor(chopper, choppee) {
        super(15);
        this.chopper = chopper;
        this.choppee = choppee;
    }

    execute() {
        let comp = this.choppee.getComponent(ChoppableComponent.name);

        comp.chop();
    }
}

/**
 * Quaff a potion from the entitys inventory if possible
 */
export class QuaffAction extends Action {
    constructor(entity) {
        super(10);
        this.entity = entity;
    }

    execute() {
        let inv = this.entity.getComponent(InventoryComponent.name);
        let atk = this.entity.getComponent(AttackableComponent.name);

        // Can only quaff if has inventory and is attackable and not at full health
        if (inv && atk && atk.hitpoints < atk.maxHitpoints) {
            let pot = inv.takeFirst("potion");

            if (pot) {
                let healed = 5;
                let txt = new FloatingTextSettings(`Health + ${healed}`);
                txt.movement = new Vector(Math.random()*40-20, 50);
                this.entity.emitFloatingText(txt);
                atk.hitpoints += healed;
                atk.hitpoints = Math.min(atk.hitpoints, atk.maxHitpoints);

                return this.cost;
            }
        }

        return 0;
    }
}

/**
 * Build a campfire and spawn night creatures!
 */
export class BuildCampfire extends Action {
    constructor(entity, targetedCell) {
        super(30);
        this.entity = entity;
        this.targetCell = targetedCell;
    }

    execute() {
        let inv = this.entity.getComponent(InventoryComponent.name);

        // Can only quaff if has inventory and is attackable and not at full health
        if (inv) {
            let logCount = inv.getItemCount("log");
            let engine = this.entity.engine;

            if (logCount >= GameRules.campfireCost && !this.targetCell.entity && GameState.instance.nightTurns == 0) {
                GameState.instance.nightTurns = GameRules.nightTurns;
                GameState.instance.nights++;

                if (GameState.instance.nights == GameRules.nightsToWin) {
                    GameState.win();
                }

                let fire = engine.entityFactory.createEntity("campfire_simple", engine, this.targetCell);
                engine.map.addEntity(this.targetCell.pos, fire);

                for (let i in Array.from({length: GameRules.campfireCost})) {
                    inv.takeFirst("log");
                }

                let txt = new FloatingTextSettings(`Lost ${GameRules.campfireCost} logs`);
                txt.movement = new Vector(Math.random()*40-20, 50);
                this.entity.emitFloatingText(txt);

                // Spawn some night creatures... the horror :O

                let pos = this.entity.cell.pos.copy();
                let added = 0;

                while (added < 1) {
                    pos.add(new Vector(parseInt(Math.random() * 15 - 5), parseInt(Math.random() * 15 - 5)));
                    let c = engine.map.getCell(pos.x, pos.y);

                    if (c && !c.entity) {
                        added++;
                        let monster = engine.entityFactory.createEntity("night_creature", engine);
                        engine.map.addEntity(new Vector(pos.x, pos.y), monster);
                    }
                }
            }
        }

        return 0;
    }
}
