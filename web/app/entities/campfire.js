"use strict";

import {Entity} from "../engine/entity";
import {GameEngine} from "../engine/engine";
import {EntityFactory} from "./entityFactory";
import {Vector} from "../engine/vector";
import {PlayerEntity, movementVectors} from "./playerEntity";
import {InventoryComponent} from "./components";
import {GameRules, GameState} from "../rules";

export class Campfire extends Entity {
    constructor(sprite, cell) {
        super(sprite, cell);
    }

    takeTurn() {
        GameState.instance.nightTurns--;

        if (GameState.instance.nightTurns <= 0) {
            this.kill();
        }

        return GameRules.campfireTurnLength;
    }
}
