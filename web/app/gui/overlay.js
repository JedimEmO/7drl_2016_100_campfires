"use strict";

import {Vector} from "../engine/vector";
import {PlayerEntity} from "../entities/playerEntity";
import {AttackableComponent} from "../engine/entity";
import {InventoryComponent}  from "../entities/components";
import {GameRules, GameState} from "../rules";

export function createGameOverlay (overlay, aaa, sprites) {
    let healthBar = sprites.getSprite("health_bar");

    let prevHp = -1;

    overlay.addWidget("player_health_widget", new Vector(20, 720-40), new Vector(200, 20), (ctx) => {

        let max = 179;
        let current = max;
        let pe = PlayerEntity.instance;

        if (pe) {
            let cmp = pe.getComponent(AttackableComponent.name);

            if (prevHp != cmp.hitpoints) {
                prevHp = cmp.hitpoints;

                current = max * (cmp.hitpoints / cmp.maxHitpoints);

                ctx.fillStyle = "black";
                ctx.fillRect(9, 5, 5 + max, 12);

                if (current > 0) {
                    ctx.fillStyle = "red";
                    ctx.fillRect(9, 5, 5 + current, 12);
                }

                ctx.drawImage(healthBar.img, 0, 0);
            }
        }
    });

    let pot = sprites.getSprite("potion");
    let log = sprites.getSprite("log");
    let invBackdrop = sprites.getSprite("inventory_bar");
    let initialized = false;

    let prevPotCount = -1;
    let prevLogCount = -1;

    overlay.addWidget("player_inventory_widget", new Vector(240, 680), new Vector(100, 20), (ctx) => {
        let inv = PlayerEntity.instance.getComponent(InventoryComponent.name);

        if (!initialized) {
            ctx.font = `bold 14px ArchitectsDaughter`;
            ctx.fillStyle = `black`;
            initialized = true;
        }

        let potCount = inv.getItemCount("potion");
        let logCount = inv.getItemCount("log");

        if (potCount != prevPotCount || logCount != prevLogCount) {
            prevLogCount = logCount;
            prevPotCount = potCount;

            ctx.save();

            ctx.drawImage(invBackdrop.img, 0, 0);
            ctx.drawImage(pot.img, 10, 2, 14, 14);
            ctx.drawImage(log.img, 50, 2, 14, 14);

            if (potCount >= 1) {
                ctx.fillStyle = "rgb(100,255,100)";
            }

            ctx.fillText(`${potCount}`, 25, 14);

            ctx.fillStyle = `black`;

            if (logCount >= GameRules.campfireCost) {
                ctx.fillStyle = "rgb(100,255,100)";
            }

            ctx.fillText(`${logCount}`, 66, 14);

            ctx.restore();
        }
    });

    let initializedScore = false;
    let prevScore = -1;
    let fire = sprites.getSprite("campfire_simple");

    overlay.addWidget("player_score_widget", new Vector(360, 680), new Vector(100, 20), (ctx) => {
        if (!initializedScore) {
            ctx.font = `bold 14px ArchitectsDaughter`;
            ctx.fillStyle = `black`;
            initializedScore = true;
        }

        if (prevScore != GameState.instance.nights) {
            prevScore = GameState.instance.nights;

            ctx.save();

            ctx.drawImage(invBackdrop.img, 0, 0);
            ctx.drawImage(fire.img, 10, 2, 14, 14);

            ctx.fillText(`${prevScore}/${GameRules.nightsToWin}`, 25, 14);

            ctx.restore();
        }
    });



    let oldGameResult = 0;
    overlay.addWidget("player_loss_widget", new Vector(1280/2-200, 720/2-200), new Vector(400, 400), (ctx) => {
        if (GameState.instance.result == -1 && oldGameResult != -1) {
            oldGameResult = -1;
            ctx.fillStyle = "rgb(150, 150, 150)";
            ctx.fillRect(0,0,400,400);

            ctx.font = `bold 14px ArchitectsDaughter`;
            ctx.fillStyle = `black`;

            ctx.fillText("You have lost. Died. Expired", 10, 25);
            ctx.fillText("The game will restart in 5 seconds.", 10, 400-4);
        }
    });

    overlay.addWidget("player_win_widget", new Vector(1280/2-200, 720/2-200), new Vector(400, 400), (ctx) => {
        if (GameState.instance.result == 1 && oldGameResult != 1) {
            oldGameResult = 1;
            ctx.fillStyle = "rgb(150, 150, 150)";
            ctx.fillRect(0,0,400,400);

            ctx.font = `bold 14px ArchitectsDaughter`;
            ctx.fillStyle = `black`;

            ctx.fillText("You have won. Congratulations", 10, 25);
            ctx.fillText("The game will restart in 5 seconds.", 10, 400-4);
        }
    });

    let oldShowHelp = false;

    overlay.addWidget("player_help_widget", new Vector(1280/2-200, 720/2-200), new Vector(400, 400), (ctx) => {
        if (GameState.instance.showHelp && !oldShowHelp) {
            oldShowHelp = true;
            ctx.fillStyle = "rgb(150, 150, 150)";
            ctx.fillRect(0,0,400,400);

            ctx.font = `bold 14px ArchitectsDaughter`;
            ctx.fillStyle = `black`;

            let y = 1;

            ctx.fillText("Survive for 100 nights", 10, 25);
            ctx.fillText("This means lighting 100 campfires.", 10, 25 + (y++) * 18);
            ctx.fillText("To do this, chop down trees to gain wood.", 10, 25 + (y++) * 18);
            ctx.fillText(`As soon as you have ${GameRules.campfireCost} logs, press 'e' or 'enter'`, 10, 25 + (y++) * 18);
            ctx.fillText("to make a bonfire.", 10, 25 + (y++) * 18);
            y++;
            ctx.fillText("Oh, and use aeds or numpad 4862", 10, 25 + (y++) * 18);
            ctx.fillText("to move around.", 10, 25 + (y++) * 18);
            y++;
            ctx.fillText("You may find some potions before you die.", 10, 25 + (y++) * 18);
            ctx.fillText("Press q to use them.", 10, 25 + (y++) * 18);
            y++;
            ctx.fillText("If you run into enemies, run headfirst into them.", 10, 25 + (y++) * 18);
            ctx.fillText("That should damage them quite a bit.", 10, 25 + (y++) * 18);
            ctx.fillText("The same goes for trees..", 10, 25 + (y++) * 18);
            y++;
            y++;
            ctx.fillText("By the way...", 10, 25 + (y++) * 18);
            y++;
            y++;
            ctx.fillText("Don't die!", 10, 25 + (y++) * 18);
        } else if (!GameState.instance.showHelp && oldShowHelp){
            oldShowHelp = false;

            ctx.clearRect(0,0,400,400);
        }
    });
}
