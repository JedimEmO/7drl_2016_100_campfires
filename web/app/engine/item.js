"use strict";

export class Item {
    constructor() {
    }

    /**
     * the entity will attempt to pick up the item
     *
     * If the entity has an inventory, the item will add itself to it
     *
     * Healing items will attempt to heal attackable entities
     *
     * @param entity
     */
    pickUp(entity) {
    }

    use(entity) {
    }
}
