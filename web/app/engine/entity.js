"use strict";

import {Actor} from "./action/actionQueue";
import {Vector} from "./vector";
import {GameEngine} from "./engine";
import {Transform} from "./map/coordinates";
import {FloatingTextSettings} from "./gfx/overlay";

export class Entity extends Actor {
    constructor(sprite, cell) {
        super();
        this.sprite = sprite;
        this.cell = cell;
        this.components = {};
        this.emitText = [];
    }

    emitFloatingText(txt) {
        this.emitText.push(txt);
    }

    onDeath() {
        this.cell.entity = null;
        super.onDeath();
    }

    getComponent(compClass) {
        return this.components[compClass];
    }

    addComponent(comp) {
        this.components[comp.constructor.name] = comp;
    }

    takeTurn() {
    }

    draw(ctx, sx, sy) {
        let dx = sx - this.physical.width/2;
        let dy = sy - this.physical.height;

        ctx.drawImage(this.sprite.img, dx, dy);

        for (let txt of this.emitText) {
            let off = new Vector(0,0);

            if (this.physical) {
                off.y = this.physical.height;
            }

            let settings = new FloatingTextSettings();

            if (txt.constructor.name == FloatingTextSettings.name) {
                settings = txt;
            } else {
                settings.text = txt;
                settings.baseSize = 15;
                settings.movement = new Vector(Math.random() * 20 - 10, 70);
            }

            let pos = Transform.unwindPosition(new Vector(sx, sy)).subtract(off);
            GameEngine.instance.overlay.addFloatingText(1500, pos, settings);
        }

        this.emitText.length = 0;
    }
}

export class Component {
    constructor(entity) {
        this.entity = entity;
    }
}

export class AttackableComponent extends Component {
    constructor(entity, hitpoints) {
        super(entity);
        this.hitpoints = hitpoints;
        this.maxHitpoints = hitpoints;
    }

    damage(points) {
        this.hitpoints -= points;

        // If hitpoints reach 0, terminate.
        if (this.hitpoints <= 0) {
            this.entity.kill();
        }
    }

    heal(points) {
        this.hitpoints = Math.min(this.hitpoints + points, this.maxHitpoints);
    }
}
