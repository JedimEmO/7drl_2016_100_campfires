"use strict";

export class Vector {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    length() {
        return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2));
    }

    subtract(vec) {
        this.x -= vec.x;
        this.y -= vec.y;

        return this;
    }

    add(vec) {
        this.x += vec.x;
        this.y += vec.y;

        return this;
    }

    mult(scalar) {
        this.x *= scalar;
        this.y *= scalar;

        return this;
    }

    copy() {
        return new Vector(this.x, this.y);
    }

    toString() {
        return `${this.x},${this.y}`;
    }

    static fromString(s) {
        let split = s.split(",");
        return new Vector(parseInt(split[0]), parseInt(split[1]));
    }

    static interpolate(vecA, vecB, pct) {
        let tmp = vecA.copy().subtract(vecB);
        return vecA.copy().add(tmp.mult(pct));
    }
}
