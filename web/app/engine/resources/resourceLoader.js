"use strict";

class SpriteSheet {
    constructor() {
        this.sprites = {};
    }

    addSprite(spr) {
        this.sprites[spr.name] = spr;
    }

    getSprite(name) {
        if (!this.sprites.hasOwnProperty(name)) {
            throw `Attempting to get invalid sprite ${name}`;
        }

        return this.sprites[name];
    }
}

export class ItemDatabase {
    constructor() {
        this.items = {};
    }

    addItem(id, item) {
        this.items[id] = item;
    }

    createItem(id) {
        if (!this.items.hasOwnProperty(id)) {
            throw `Attempting to create item from invalid id ${id}`;
        }

        return Object.assign({}, this.items[id]);
    }
}

export class ResourceLoader {
    constructor(resourcePath) {
        this.resourcePath = resourcePath;
    }

    loadImage(sprite) {
        let ret = new Image();
        ret.src = this.resourcePath + "/sprites/" + sprite.filename;

        return ret;
    }

    /**
     * Loads all the sprites from the provided sprite definition object
     * Returns a promise that resolves when all the images are loaded.
     *
     * @param spriteDefinitionObject
     * @returns {*}
     */
    loadSprites(spriteDefinitionObject) {
        let sprites = new SpriteSheet();
        let promises = [];

        for (let spr of spriteDefinitionObject.sprites) {
            spr.img = this.loadImage(spr);
            spr.height = spr.height || 0;
            spr.left = spr.left || 0;

            let p = new Promise((resolve, reject) => {
                spr.img.onload = () => {
                    resolve();
                };
            });

            promises.push(p);

            sprites.addSprite(spr);
        }

        return Promise.all(promises).then(() => { return sprites; });
    }

    /**
     * Load all images from the tileset.
     *
     * @param tileDefinitionObject
     * @param spriteSet
     * @returns tileset
     */
    loadTileset(tileDefinitionObject, spriteSet) {
        let tiles = {};

        for (let tile of tileDefinitionObject.tiles) {
            tile.sprite = spriteSet.getSprite(tile.sprite);

            tiles[tile.name] = tile;
        }

        return tiles;
    }

    loadItems(itemData, spriteSet) {
        let database = new ItemDatabase();

        for (let item of Object.keys(itemData.items)) {
            let itm = itemData.items[item];
            itm.sprite = spriteSet.getSprite(itm.sprite);
            database.addItem(item, itm);
        }

        return database;
    }
}
