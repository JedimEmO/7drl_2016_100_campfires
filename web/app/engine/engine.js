"use strict";

import {MapChunk} from "app/engine/map/chunk";
import {Map} from "app/engine/map/map";
import {EntityFactory,AttackableComponent} from "app/engine/entity";
import {Vector} from "app/engine/vector";
import {Action,ActionQueue} from "app/engine/action/actionQueue";
import {PlayerEntity} from "app/entities/playerEntity";
import {GameOverlay} from "./gfx/overlay";

export class GameEngine
{
    /**
     * Sets up the game engine to render on the provided canvas
     *
     * @param domCanvas Rendering target canvas
     * @param tileset The tileset object used by the game
     * @param sprites
     * @param entityFactory
     * @param logger
     */
    constructor(domCanvas, tileset, sprites, entityFactory, itemDatabase, onBeforeStart, createGameOverlay, logger) {
        GameEngine.instance = this;
        this.sprites = sprites;
        this.itemDatabase = itemDatabase;
        this.canvas = domCanvas;
        this.tileset = tileset;
        this.entityFactory = entityFactory;
        this.onBeforeStart = onBeforeStart;
        this.logger = logger;

        this.ctx = this.canvas.getContext("2d");
        this.overlay = new GameOverlay(this.ctx);

        createGameOverlay(this.overlay, this.ctx, sprites);

        this.config = {
            "fps": 30
        };
    }

    /**
     * Starts executing the main game loop
     */
    run() {
        this.logger.log("Starting execution of main game loop");

        this.map = new Map(this, new Vector(25, 25));

        // Invoke the onStart callback
        this.onBeforeStart(this);

        ActionQueue.restart();
        this.startGameLoop();
    }

    stop() {
        window.cancelAnimationFrame(this.animationRequest);
    }

    /**
     * Progress the game logic
     */
    stepLogic() {
        let blockingCount = 0;

        for (let crKey of Object.keys(this.map.chunks)) {
            let cr = this.map.chunks[crKey];

            for (let cKey of Object.keys(cr)) {
                blockingCount += cr[cKey].actionQueue.step();
            }
        }

        if (blockingCount == 0) {
            ActionQueue.tick++;
        }
    }

    removeDeadActors() {
        for (let crKey of Object.keys(this.map.chunks)) {
            let cr = this.map.chunks[crKey];

            for (let cKey of Object.keys(cr)) {
                cr[cKey].actionQueue.removeDead();
            }
        }
    }

    startGameLoop() {
        let step = (ts) => {
            this.animationRequest = window.requestAnimationFrame(step);

            this.removeDeadActors();
            this.stepLogic();

            this.draw();
            this.overlay.draw();
        };

        step();
    }

    draw() {
        this.map.draw(this.ctx);
    }
}
