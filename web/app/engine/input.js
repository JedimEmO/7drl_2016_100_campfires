"use strict";

export class KeyboardInput {
    constructor() {
        this._state = {};
        this.rate = 80;

        this.inputKeyGroups = {
            up: [104, 87], // w, numpad 8
            left: [65, 100], // a, numapd 4
            right: [68, 102], // d, numapd 6
            down: [98, 83], // // s, numapd 2
            action: [69, 13], // e, enter,
            quaff: [81], // q
            help: [72] // h
        };

        window.addEventListener("keyup", (event) => { this.onKeyUp(event); });
        window.addEventListener("keydown", (event) => { this.onKeyDown(event); });
    }

    /**
     * Returns true if one of the keys in the code set is pressed
     *
     * @param keyCodes
     * @returns {boolean}
     */
    isAnyPressed(keyCodes) {
        let ret = false;

        for (let code of keyCodes) {
            ret |= this._state[code]
        }

        return ret;
    }

    timeSinceKeydown(code) {
        let epoch = (new Date).getTime();

        if (this._state.hasOwnProperty([code])) {
            return epoch - (this._state[code] || 0);
        }

        return epoch;
    }

    onKeyDown(event) {
        if (this.timeSinceKeydown(event.keyCode) > this.rate) {
            this._state[event.keyCode] = (new Date).getTime();
        }
    }

    onKeyUp(event) {
        delete this._state[event.keyCode];
    }
}
