"use strict";

import {Vector} from "app/engine/vector";
import {ActionQueue} from "app/engine/action/actionQueue";
import {mapToScreen, Transform} from "app/engine/map/coordinates";
import {GameEngine} from "app/engine/engine";
import {Item} from "../item";

function nextTile(pt) {
    return {y: pt.y -1, x: pt.x + 1, num: pt.num + 1};
}

/**
 * Cell representing the smallest unit in the game world (tile sized)
 * Contains all the tile layers
 */
class Cell {
    /**
     * @param tile
     * @param pos
     * @param entity
     */
    constructor(tile, pos, map) {
        this.tile = tile;
        this.decorations = [];
        this.items = [];
        this.overlays = {};// Overlays drawn on top of the tile - indicators, highlights etc.
        this.map = map;
        this.entity = null;
        this.pos = new Vector(pos.x, pos.y);

    }

    addOverlay(id, overlay) {
        this.overlays[id] = overlay;
    }

    removeOverlay(id) {
        delete this.overlays[id];
    }

    draw(ctx, sx, sy) {
        // Profit?
        ctx.drawImage(this.tile.sprite.img, sx, sy);

        for (let dec of this.decorations) {
            ctx.drawImage(dec.img, sx, sy);
        }

        for (let it of this.items) {
            ctx.drawImage(it.sprite.img, sx+8, sy+8);
        }

        // Render the entity at the center of the tile, if there is any
        if (this.entity) {
            sx += 32;
            sy += 24;

            this.entity.draw(ctx, sx, sy);
        }

        for (let over of Object.keys(this.overlays)) {
            let pos = Transform.unwindPosition(new Vector(sx, sy));
            GameEngine.instance.overlay.setSprite(over, this.overlays[over], pos);
        }
    }
}

// Place up to 5 trees in a group
function placeTreeGroup(map, engine, center, aq) {
    let ef = engine.entityFactory;
    let maxTrees = 5;
    let numTrees = parseInt(Math.random() * (maxTrees -1 )) + 1;

    let nominees = [];

    for (let i in Array.from({length: 9})) {
        nominees.push(new Vector(-1 + i % 3, -1 + i % 3).add(center));
    }

    let added = 0;

    while (added < numTrees) {
        let grabIdx = parseInt(Math.random() * nominees.length);
        let p = nominees[grabIdx];

        nominees.splice(0, 1);

        let cell = map[p.y][p.x];

        if (!cell.entity) {
            cell.entity = ef.createEntity("tree_1", engine, map[p.y][p.x]);
            aq.addActor(cell.entity);
        }

        added++;
    }
}

export class MapChunk {
    constructor(engine, width, height, chunkStart) {
        this.engine = engine;

        this.width = width;
        this.height = height;

        this.chunkStart = chunkStart;
        this.actionQueue = new ActionQueue();

        this.setupTest(engine, width, height, chunkStart);
    }

    setupTest(engine, width, height, chunkStart) {
        // Testing code follows: this needs to go away
        let grass = this.engine.tileset["grass_1"];
        let flowers = this.engine.sprites.getSprite("flowers");
        let stone = this.engine.sprites.getSprite("stone");
        let reeds = this.engine.sprites.getSprite("reeds");

        // Create a 2d-array matching the map size
        this.map = Array.from({length: width}, (v, y) =>
            Array.from({length: height}, ((v, x) => {
                let c = new Cell(grass, chunkStart.copy().add(new Vector(x, y)), this.map);

                if (Math.random() * 75 <= 1 && c.decorations.length == 0) {
                    if (Math.random() * 2 < 1) {
                        c.decorations.push(flowers);
                    } else {
                        c.decorations.push(stone);
                    }
                }

                if (Math.random() * 200 <= 1 && c.decorations.length == 0) {
                    c.decorations.push(reeds);
                }

                return c;
            })));

        let groups = parseInt(Math.random() * 9) + 3;

        for (let i in Array.from({length: groups})) {
            let p = new Vector(parseInt(Math.random() * (this.width - 9)) + 3,
                parseInt(Math.random() * (this.height - 9)) + 3);

            placeTreeGroup(this.map, engine, p, this.actionQueue);
        }
    }

    getCell(x, y) {
        if (x < 0 || x >= this.width) {
            return null;
        }
        if (y < 0 || y >= this.height) {
            return null;
        }

        return this.map[y][x];
    }

    // Get the tile that is the first to be drawn on a given y-value
    startTile(line) {
        let pt = {x: 0, y: line, num: 0};

        if (pt.y >= this.height) {
            pt.y = this.height -1;
            pt.x = line - pt.y;
        }

        return pt;
    }

    drawCell(ctx, cell, pt, line) {
        let off = line - pt.y;

        let sx = (pt.y - off) * 48;
        let sy = line * 24;

        cell.draw(ctx, sx, sy);
    }

    drawLine(ctx, lineNum) {
        let pt = this.startTile(lineNum);

        // Go through the line across the screen
        while (pt.x < this.width && pt.y >= 0) {
            let cell = this.map[pt.y][pt.x];

            this.drawCell(ctx, cell, pt, lineNum);

            pt = nextTile(pt); // Advance a tile on the line
        }
    }

    draw(ctx) {
        for (let line = 0; line < this.height*2; line++) {
            this.drawLine(ctx, line);
        }
    }
}
