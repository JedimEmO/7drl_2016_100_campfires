"use strict";

import {MapChunk} from "app/engine/map/chunk";
import {mapToScreen, Transform} from "app/engine/map/coordinates";
import {Vector} from "app/engine/vector";
import {GameRules, GameState} from "../../rules";

export class Map {
    constructor(engine, chunkSize) {
        this.engine = engine;
        this.chunkSize = chunkSize;
        this.chunks = {};

        // Some temporary testing config
        this.setupTest(engine, chunkSize);
    }

    setupTest(engine, chunkSize) {

        this.chunks = {
            0: {
                0: new MapChunk(engine, chunkSize.x, chunkSize.y, new Vector(0, 0)),
                1: new MapChunk(engine, chunkSize.x, chunkSize.y, new Vector(chunkSize.x, 0)),
                2: new MapChunk(engine, chunkSize.x, chunkSize.y, new Vector(chunkSize.x*2, 0))
            },
            1: {
                0: new MapChunk(engine, chunkSize.x, chunkSize.y, new Vector(0, chunkSize.y)),
                1: new MapChunk(engine, chunkSize.x, chunkSize.y, new Vector(chunkSize.x, chunkSize.y)),
                2: new MapChunk(engine, chunkSize.x, chunkSize.y, new Vector(chunkSize.x*2, chunkSize.y))
            },
            2: {
                0: new MapChunk(engine, chunkSize.x, chunkSize.y, new Vector(0, chunkSize.y*2)),
                1: new MapChunk(engine, chunkSize.x, chunkSize.y, new Vector(chunkSize.x, chunkSize.y*2)),
                2: new MapChunk(engine, chunkSize.x, chunkSize.y, new Vector(chunkSize.x*2, chunkSize.y*2))
            }
        };

        let player = engine.entityFactory.createEntity("player_1", engine);
        this.addEntity(new Vector(25,25), player);
        engine.playerEntity = player;
    }

    getChunk(x, y) {
        if (this.chunks.hasOwnProperty(y) && this.chunks[y].hasOwnProperty(x)) {
            return this.chunks[y][x];
        }
// experimental: add chunk instead of returning null!

        if (x >= 0 && y >= 0) {
            this.chunks[y] = this.chunks[y] || {};
            this.chunks[y][x] = new MapChunk(this.engine, this.chunkSize.x, this.chunkSize.y, new Vector(this.chunkSize.x * x, this.chunkSize.y * y));

            return this.chunks[y][x];
        }

        return null;
    }

    getChunkFromWorldCoords(x, y) {
        return this.getChunk(parseInt(x/this.chunkSize.x), parseInt(y/this.chunkSize.y));
    }

    getCell(x, y) {
        let chunkX  = parseInt(x / this.chunkSize.x);
        let chunkY =  parseInt(y / this.chunkSize.y);

        let chunk = this.getChunk(chunkX, chunkY);

        if (chunk) {
            // Get the cell in coordinates within the chunk
            let cellX = x - chunkX * this.chunkSize.x;
            let cellY = y - chunkY * this.chunkSize.y;

            return chunk.getCell(cellX, cellY);
        }
    }

    addEntity(pos, entity) {
        let cell = this.getCell(pos.x, pos.y);

        if (cell) {
            let chunk = this.getChunkFromWorldCoords(pos.x, pos.y);
            // Add the entity to this maps action queue
            chunk.actionQueue.addActor(entity);

            cell.entity = entity;
            entity.cell = cell;
        }
    }

    /**
     * Draws the chunks surrounding the player. Hightly hackish code ahead, but should do the trick for
     * 7drl purposes.
     *
     * @param ctx
     */
    draw(ctx) {
        let pe = this.engine.playerEntity;
        let offset = new Vector(0,0);

        let startChunk = new Vector(0,0);

        let linesToRender = 8;
        let dOriginOffset = new Vector(-2, -2); // Start drawing two chunks straight up from the current player chunk
        let dStart = new Vector(0, 1); // The start chunk changes with y+1
        let dNext = new Vector(1, -1); // Moving right along the line is +1x, -1y

        ctx.save();
        ctx.setTransform(1, 0, 0, 1, 0, 0);

        // Last hours hack: make it look dark.
        if (GameState.instance.nightTurns > 0) {
            ctx.fillStyle = "rgb(0, 0, 40)";
            ctx.fillRect(0, 0, 1280, 720);
            ctx.globalAlpha = 0.4;
        } else {
            ctx.clearRect(0, 0, 1280, 720);
            ctx.globalAlpha = 1;
        }

        if (pe) {
            let playerChunkPos = new Vector(parseInt(pe.cell.pos.x/this.chunkSize.x), parseInt(pe.cell.pos.y/this.chunkSize.y));

            offset = mapToScreen(pe.cell.pos).subtract(new Vector(1280/2, 720/2));
            startChunk = playerChunkPos.add(dOriginOffset);
        }

        Transform.pushTranslate(ctx, -offset.x, -offset.y);

        let currentLineStart = startChunk;

        for (let n = 0; n < linesToRender; ++n) {

            let current = currentLineStart.copy();

            for (let c = 0; c < n+1; ++c) {
                let chunk = this.getChunk(current.x, current.y);

                if (chunk) {
                    let chunkOff = mapToScreen(new Vector(current.x * this.chunkSize.x, current.y * this.chunkSize.y));

                    ctx.save();
                    Transform.pushTranslate(ctx, chunkOff.x, chunkOff.y);

                    chunk.draw(ctx);

                    Transform.popTranslate();
                    ctx.restore();
                }

                current.add(dNext);
            }

            currentLineStart.add(dStart); // Move the line start
        }

        Transform.popTranslate();
        ctx.restore();
    }
}
