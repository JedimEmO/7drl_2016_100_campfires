"use strict";

import {Vector} from "app/engine/vector";

export function mapToScreen(vec) {
    let px = -vec.x * 48 + vec.y * 48;
    let py = vec.x * 24 + vec.y * 24;

    return new Vector(px, py);
}

export class Transform {
    static unwindPosition(pos) {
        return pos.copy().add(Transform.currentTranslate);
    }

    static pushTranslate(ctx, x, y) {
        ctx.translate(x, y);

        Transform.translates.push(new Vector(x,y));
        Transform.currentTranslate.add(Transform.translates[Transform.translates.length - 1]);
    }

    static popTranslate() {
        Transform.currentTranslate.subtract(Transform.translates[Transform.translates.length - 1]);
        Transform.translates.splice(Transform.translates.length - 1, 1);
    }
}

Transform.translates = [];
Transform.currentTranslate = new Vector(0,0);
