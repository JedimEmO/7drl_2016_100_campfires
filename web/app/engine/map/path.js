"use strict";

import {mapToScreen} from "./coordinates";
import {Vector} from "app/engine/vector";


function heuristic(start, end) {
    let ax = start.x - Math.floor(start.y/2);
    let ay = start.x + Math.ceil(start.y/2);

    let bx = end.x - Math.floor(end.y/2);
    let by = end.x + Math.ceil(end.y/2);

    let d = new Vector(bx - ax, by - ay);

    let dist = 0;

    if (Math.sign(d.x) == Math.sign(d.y)) {
        dist = Math.max(Math.abs(d.x), Math.abs(d.y));
    } else {
        dist = Math.abs(d.x) + Math.abs(d.y);
    }

    return dist;
}

function getLowestCostNode(list, map) {
    let lowest = Number.MAX_VALUE;
    let ret;

    for (let n of list) {
        let c = map.get(n.toString());

        if (c < lowest) {
            ret = n;
            lowest = c;
        }
    }

    return ret;
}

function constructResult(path, n) {
    if (path.has(n.toString())) {
        let p = constructResult(path, path.get(n.toString()));
        p.push(n);

        return p;
    } else {
        return [n];
    }
}

let siblings = [
    new Vector(-1, 0),
    new Vector(-1, -1),
    new Vector(0, -1),
    new Vector(1, 0),
    new Vector(1, 1),
    new Vector(0, 1)
];

/**
 * Calculates a path from start -> end in the map if possible. the map parameter
 * must have a getCell() function that takes x and y coordinates. start and end must
 * be 2d vectors
 *
 * @param map
 * @param start
 * @param end
 * @param passableCb
 * @param limit
 */
export function findPath(map, start, end, passableCb, limit) {
    let open = new Set();
    let closed = new Set();
    let count = 0;

    let scores = new Map();
    let estimates = new Map();
    let path = new Map();

    open.add(start.toString());

    scores.set(start.toString(), 0);
    estimates.set(start.toString(), heuristic(start, end));

    while (open.size) {
        if (limit && count > limit) {
            return [];
        }

        let current = Vector.fromString(getLowestCostNode(open, estimates));
        let cs = current.toString();
        
        if (cs == end.toString()) {
            return constructResult(path, end);
        }

        open.delete(cs);
        closed.add(cs);

        for (let s of siblings) {
            let siblingPos = s.copy().add(current);
            let sps = siblingPos.toString();
            
            let valid = passableCb(map, siblingPos);

            if (valid && !closed.has(sps)) {
                let total = scores.get(current.toString()) + 1;

                if (!open.has(sps) || total < scores.get(sps)) {
                    path.set(sps, current);
                    scores.set(sps, total);
                    estimates.set(sps, total + heuristic(siblingPos, end));

                    if (!open.has(sps)) {
                        open.add(sps);
                    }
                }
            }
        }

        count++;
    }

    return [];
}
