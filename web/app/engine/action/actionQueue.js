"use strict";

export class Action {
    constructor(cost) {
        this.cost = cost;
    }

    execute() {
        console.log("Executing non-action (default execute handler)");
    }
}

export class Actor {
    constructor() {
        this.nextTurn = 0;
        this.alive = true; // If false, the actor will be removed after the tick
    }

    kill() {
        this.alive = false;
    }

    onDeath() {
    }

    /**
     * A blocking actor will prevent the tick from progressing until it has taken its turn
     * @returns {boolean}
     */
    isBlocking() {
        return false;
    }

    takeTurn() {
        return 10;
    }
}

/**
 * Class governing the stepping of actions
 */
export class ActionQueue {

    constructor() {
        this.actors = [];
        this.blockingActors = [];
    }

    static stop() {
        ActionQueue.running = false;
    }

    static restart() {
        ActionQueue.tick = 0;
        ActionQueue.running = true;
    }

    addActor(actor) {
        if (actor.isBlocking()) {
            this.blockingActors.push(actor);
        } else {
            this.actors.push(actor);
        }
    }

    getActiveBlockingActors() {
        let num = 0;

        for (let a of this.blockingActors) {
            if (a.nextTurn <= ActionQueue.tick) {
                num++;
            }
        }

        return num;
    }

    /**
     * Have all the actors in the list take their turn
     * @param actors
     */
    static stepActors(actors) {
        for (let actor of actors) {
            if (ActionQueue.tick >= actor.nextTurn) {
                actor.nextTurn = ActionQueue.tick + actor.takeTurn();
            }
        }
    }

    static removeDeadFromList(actors) {
        for (let a in Object.keys(actors)) {
            let actor = actors[a];

            if (actor && !actor.alive) {
                actor.onDeath();
                actors.splice(a, 1);
            }
        }
    }

    removeDead() {
        //TODO: We probably don't want to remove the player entity this way... ActionQueue.removeDeadFromList(this.blockingActors);
        ActionQueue.removeDeadFromList(this.actors);
    }

    step() {
        if (ActionQueue.running) {
            if (this.getActiveBlockingActors() > 0) {
                ActionQueue.stepActors(this.blockingActors);
            } else {
                ActionQueue.stepActors(this.actors);
            }
        }

        // Return the number of still active blocking actors
        return this.getActiveBlockingActors();
    }
}

ActionQueue.tick = 0;
ActionQueue.running = false;
