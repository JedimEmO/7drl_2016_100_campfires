"use strict";

import {Vector} from "../vector";

/**
 * Widgets can be added to the overlay and render custom graphics on top of the canvas
 */
class Widget {
    constructor(position, size, cb) {
        this.position = position;
        this.size = size;
        this.cb = cb;
        // Create a separate canvas for the widgets - to avoid resetting fonts all the time
        this.canvas = document.createElement("canvas");
        this.context = this.canvas.getContext("2d");
        this.canvas.width = size.x;
        this.canvas.height = size.y;
    }

    draw(ctx) {
        ctx.save();
        ctx.translate(this.position.x, this.position.y);

        // Render to the internal canvas
        this.cb(this.context);

        // Then blit it to the main canvas
        ctx.drawImage(this.canvas, 0, 0);

        ctx.restore();
    }
}

class FloatingText {
    constructor(position, life, settings) {
        this.settings = settings;
        this.position = position;
        this.life = life;
        this.lastUpdate = (new Date).getTime();
        this.start = this.lastUpdate;
        this.endOfLife = life + this.lastUpdate;
        this.alive = true;

        // Create a separate canvas for the floating texts - to avoid resetting fonts all the time
        this.canvas = document.createElement("canvas");
        this.canvas.width = settings.text.length * settings.baseSize;
        this.canvas.height = settings.baseSize;
        this.context = this.canvas.getContext("2d");

        // pre-render the text
        this.settings.setCtx(this.context, 0);
        this.context.fillText(this.settings.text, 0, settings.baseSize-2);
    }

    draw(ctx) {
        ctx.save();
        let epoch = (new Date).getTime();
        let dt = epoch - this.lastUpdate;
        this.lastUpdate = epoch;

        if (epoch > this.endOfLife) {
            this.alive = false;
        }

        let progress = (epoch - this.start)/(this.endOfLife - this.start);

        let width = ctx.measureText(this.settings.text).width;
        let dx = width / 2;

        // Speed is in pixels per second
        this.position.x -= this.settings.movement.x * (dt/1000);
        this.position.y -= this.settings.movement.y * (dt/1000);

        ctx.drawImage(this.canvas, this.position.x - dx*(1 + progress), this.position.y, this.canvas.width*(1 + progress), this.canvas.height*(1 + progress));

        ctx.restore();
    }
}

class SpriteOverlay {
    constructor(sprite, position) {
        this.sprite = sprite;
        this.position = position;
    }

    draw(ctx) {
        ctx.drawImage(this.sprite.img, this.position.x, this.position.y);
    }
}

export class FloatingTextSettings {
    constructor(text) {
        this.text = text;
        this.movement = new Vector(0, -50);
        this.baseSize = 15;
        this.rgba = [255, 0, 0];
        this.setCtx = (ctx, progress) => {
            let size = (progress) * 42 + this.baseSize;

            ctx.font = `bold ${size}px ArchitectsDaughter`;
            ctx.fillStyle = `rgba(${this.rgba[0]}, ${this.rgba[1]}, ${this.rgba[2]}, ${1 - progress})`;
        };
    }
}
export class GameOverlay {
    constructor(ctx) {
        this.ctx = ctx;
        this.widgets = {};
        this.floatingText = [];
        this.sprites = {};
    }

    draw() {
        // Render all widgets
        for (let widget of Object.keys(this.widgets)) {
            this.widgets[widget].draw(this.ctx);
        }

        for (let txt of Object.keys(this.floatingText)) {
            this.floatingText[txt].draw(this.ctx);
        }

        for (let txt of Object.keys(this.floatingText)) {
            let obj = this.floatingText[txt];

            if (obj && !obj.alive) {
                this.floatingText.splice(txt, 1);
            }
        }

        // Render the overlaid sprites
        for (let s of Object.keys(this.sprites)) {
            this.sprites[s].draw(this.ctx);
        }
    }


    setSprite(id, sprite, position) {
        if (this.sprites.hasOwnProperty(id)) {
            this.sprites[id].position = position;
            this.sprites[id].sprite = sprite;
        } else {
            this.sprites[id] = new SpriteOverlay(sprite, position);
        }
    }

    removeSprite(id) {
        delete this.sprites[id];
    }

    addFloatingText(time, position, settings) {
        if (settings.constructor.name != FloatingTextSettings.name) {
            settings = new FloatingTextSettings(settings);
        }

        this.floatingText.push(new FloatingText(position, time, settings));
    }

    /**
     * Adds a widget at the specified position. renderCb is invoked with the context
     *
     * @param id
     * @param position
     * @param size
     * @param renderCb
     */
    addWidget(id, position, size, renderCb) {
        this.widgets[id] = new Widget(position, size, renderCb);
    }
}
