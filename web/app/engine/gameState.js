"use strict";

export class GameState {
    constructor() {
    }

    /**
     * Renders the current game state to the provided context
     * @param ctx
     */
    render(ctx) {
    }
}
