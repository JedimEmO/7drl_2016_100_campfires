"use strict";

import {GameEngine} from "./engine/engine";
import {ResourceLoader} from "./engine/resources/resourceLoader";
import {EntityFactory} from "./entities/entityFactory";
import {ItemFactory} from "./entities/itemFactory";
import {GameState} from "./rules";

import "whatwg-fetch";

let r = new ResourceLoader("/resources/");

Promise.all([
    fetch("resources/sprites/sprites.json").then(response => {
        return response.json().then((data) => {
            return r.loadSprites(data);
        });
    })
]).then(values => {
    let sprites = values[0];

    Promise.all([
        fetch("resources/sprites/tiles/tiles.json").then(response => {
            return response.json().then((data) => {
                return r.loadTileset(data, sprites);
            });
        }),
        fetch("resources/entities.json").then(response => {
            return response.json().then((data) => {
                return new EntityFactory(data);
            });
        }),
        fetch("resources/items.json").then(response => {
            return response.json().then((data) => {
                return new ItemFactory(r.loadItems(data, sprites));
            });
        })
    ]).then((resources)=>{
        console.log("Loaded all resources, starting game engine");

        // Bootstrap the game!
        let gameState = new GameState(resources[0], sprites, resources[1], resources[2]);
    });
});
