#! /bin/bash

docker stop 100_campfires_server
docker rm 100_campfires_server

docker run  -d -it -p 8080:80 -v $1/web:/usr/share/nginx/html --name 100_campfires_server 100_campfires_server_image 
