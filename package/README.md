# Docker game packaging

## Building the image
Run build.sh

## Deployment
Build the image, then execute run.sh

## Development
Build the image, then execute runDev.sh with the checkout directory as a paramter. This will mount the web folder into
the nginx servers www directory, making for a rapid development cycle.
