#! /bin/bash

docker stop 100_campfires_server
docker rm 100_campfires_server

docker run  -d -it -p 8080:80 --name 100_campfires_server 100_campfires_server_image 
